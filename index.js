const express = require('express')

const app = express()

app.get('*', (req, res) =>{
 res.send('hello')
})
app.post('*', (req, res) =>{
 res.send('hello')
})
app.put('*', (req, res) =>{
 res.send('hello')
})
app.patch('*', (req, res) =>{
 res.send('hello')
})
app.delete('*', (req, res) =>{
 res.send('hello')
})

app.listen(5005, () => {
 console.log('running on 5005')
})